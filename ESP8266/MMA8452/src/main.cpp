#include <Arduino.h>
#include <Wire.h>
#include <EEPROM.h>
#include <math.h>
#include "SparkFun_MMA8452Q.h"

#define PAUSE 2000
#define TAILLE 3
#define ADRX 0
#define ADRY 4
#define ADRZ 8

/* Branchements
  3.3V -> 3.3V
  GND -> GND
  SDA -> D2
  SCL -> D1
*/

MMA8452Q accel;   // objet de la classe MMA8452

void EcrireXYZEEPROM() {    // écriture de X, Y et Z dans l'EEPROM
  EEPROM.begin(12); // début session EEPROM
  // mesures de X, Y et Z
  float vectMesure[3] = {accel.getCalculatedX(), accel.getCalculatedY(), accel.getCalculatedZ()};
  // écriture dans l'EEPROM
  EEPROM.put(ADRX, vectMesure[0]);
  EEPROM.put(ADRY, vectMesure[1]);
  EEPROM.put(ADRZ, vectMesure[2]);
  EEPROM.commit();
  EEPROM.end();     // fin de session
}

void LireXYZEEPROM(float prmVectLecture[]) {      // retourne X, Y et Z dans l'EEPROM dans un vecteur
  EEPROM.begin(12); // début session EEPROM
  // lecture de X, Y et Z dans l'EEPROM
  EEPROM.get(ADRX, prmVectLecture[0]);
  EEPROM.get(ADRY, prmVectLecture[1]);
  EEPROM.get(ADRZ, prmVectLecture[2]);
  EEPROM.end();   // fin de session
}

void AfficherXYZ(float prmVect[]) {   // affiche les données X, Y et Z d'un vecteur en paramètre
  // Affichage
  Serial.println("Valeurs enregistrées :");
  Serial.print("X = ");
  Serial.print(prmVect[0], 3);
  Serial.print("\tY = ");
  Serial.print(prmVect[1], 3);
  Serial.print("\tZ = ");
  Serial.println(prmVect[2], 3);
}

void MesurerXYZ(float prmVectMesure[]) {    // mesure X, Y et Z dans un vecteur
  if (accel.available()) {      // s'il y a une nouvelle mesure
    // écriture de X, Y et Z dans le vecteur
    prmVectMesure[0] = accel.getCalculatedX();
    prmVectMesure[1] = accel.getCalculatedY();
    prmVectMesure[2] = accel.getCalculatedZ();
  }
}

bool VerifInclinaison(float prmVectMesure[], float prmVectEEPROM[]) {   // détecte une inclinaison
  bool etatIncli = false;

  float valXEEPROM = prmVectEEPROM[0],
        valYEEPROM = prmVectEEPROM[1],
        valXMesure = prmVectMesure[0],
        valYMesure = prmVectMesure[1],
        valZMesure = prmVectMesure[2],
        XY = 0,
        angle = 0;

  XY = pow((valXMesure - valXEEPROM), 2);
  XY += pow((valYMesure - valYEEPROM), 2);
  XY = sqrt(XY) / valZMesure;
  angle = atan(XY);
  angle = (angle * 180)/PI;

  Serial.print("angle :\t");
  Serial.print(angle, 3);
  Serial.println("°Q");

  if(angle > 8) {
    etatIncli = true;
  }

  return etatIncli;
}

void VerifConnexion() {
  if (accel.begin() == false) {
    Serial.println("Not Connected. Please check connections and read the hookup guide.");
    while (1);
  }
}

void setup() {
  Serial.begin(9600);
  Wire.begin();
  VerifConnexion();
  EcrireXYZEEPROM();
}

void loop() {
  float vectMesure[TAILLE] = {0,0,0};
  float vectEEPROM[TAILLE] = {0,0,0};
  LireXYZEEPROM(vectEEPROM);
  MesurerXYZ(vectMesure);
  if (VerifInclinaison(vectMesure, vectEEPROM) == true) {
    Serial.println("Hydrant renversé !");
  }
  delay(PAUSE);
}