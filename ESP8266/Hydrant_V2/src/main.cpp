#include <Arduino.h>
#include <Wire.h>
#include <EEPROM.h>
#include <math.h>
#include "SparkFun_MMA8452Q.h"
#include "SparkFunTMP102.h"
//#include "UtilHydrant.h"

// broches ESP8266
#define INTEAU 14
#define INTMES 12
#define LED 15
// veille de 1 minute
#define VEILLE 6E7
// tailles vecteurs
#define TAILLE_TRAME 10
#define TAILLE_XYZ 3
// adresses
#define ADRMES 0
#define ADRX 10
#define ADRY 14
#define ADRZ 18


// Branchements TMP102
// VCC = 3.3V
// GND = GND
// SDA = D2
// SCL = D1

// Branchements MMA8452
// 3.3V = 3.3V
// GND = GND
// SDA = D2
// SCL = D1

// objets des classes des capteurs
TMP102 CaptTemp;
MMA8452Q Accel;

// fonctions accéléromètre
void VerifConnexionMMA8452()
{
  if (Accel.begin() == false)
  {
    Serial.println("MMA8452 non connecté !");
    while (1)
      ;
  }
  Serial.println("MMA8452 connecté !");
}

void EcrireXYZEEPROM() // écriture de X, Y et Z dans l'EEPROM
{
  EEPROM.begin(12); // début session EEPROM
  // mesures de X, Y et Z
  float vectMesure[3] = {Accel.getCalculatedX(), Accel.getCalculatedY(), Accel.getCalculatedZ()};
  // écriture dans l'EEPROM
  EEPROM.put(ADRX, vectMesure[0]);
  EEPROM.put(ADRY, vectMesure[1]);
  EEPROM.put(ADRZ, vectMesure[2]);
  EEPROM.commit();
  EEPROM.end(); // fin de session
}

void LireXYZEEPROM(float prmVectLecture[]) // retourne X, Y et Z dans l'EEPROM dans un vecteur
{
  EEPROM.begin(12); // début session EEPROM
  // lecture de X, Y et Z dans l'EEPROM
  EEPROM.get(ADRX, prmVectLecture[0]);
  EEPROM.get(ADRY, prmVectLecture[1]);
  EEPROM.get(ADRZ, prmVectLecture[2]);
  EEPROM.end(); // fin de session
}

void MesurerXYZ(float prmVectMesure[]) // mesure X, Y et Z dans un vecteur
{
  if (Accel.available()) // s'il y a une nouvelle mesure
  {
    // écriture de X, Y et Z dans le vecteur
    prmVectMesure[0] = Accel.getCalculatedX();
    prmVectMesure[1] = Accel.getCalculatedY();
    prmVectMesure[2] = Accel.getCalculatedZ();
  }
}

bool VerifInclinaison(float prmVectMesure[], float prmVectEEPROM[]) // détecte une inclinaison
{
  bool etatInclinaison = false;
  float valXEEPROM = prmVectEEPROM[0],
        valYEEPROM = prmVectEEPROM[1],
        valXMesure = prmVectMesure[0],
        valYMesure = prmVectMesure[1],
        valZMesure = prmVectMesure[2],
        XY = 0,
        angle = 0;
  // calcul de l'angle
  XY = pow((valXMesure - valXEEPROM), 2);
  XY += pow((valYMesure - valYEEPROM), 2);
  XY = sqrt(XY) / valZMesure;
  angle = atan(XY);
  angle = (angle * 180) / PI;
  // affichage
  Serial.print("Angle :\t\t");
  Serial.print(angle, 2);
  Serial.println("°");
  // vérifie si l'hydrant est renversé
  if (angle > 8)
  {
    Serial.println("Hydrant renversé !");
    etatInclinaison = true;
  }

  return etatInclinaison;
}

// fonctions mise en service
bool VerifMisEnService()
{
  bool etatMES = 0;
  EEPROM.begin(1);             // début session EEPROM
  EEPROM.get(ADRMES, etatMES); // lit la mis en service
  EEPROM.end();                // fin de session EEPROM
  return etatMES;
}

void MiseEnService()
{
  EcrireXYZEEPROM();
  EEPROM.begin(1);          // début de session EEPROM
  EEPROM.put(ADRMES, true); // écriture de l'état "mis en service"
  EEPROM.commit();          // effectue les écritures
  EEPROM.end();             // fin de session
}

void EnleverMES()
{
  EEPROM.begin(1);           // début de session EEPROM
  EEPROM.put(ADRMES, false); // écriture de l'état "mis en service"
  EEPROM.commit();           // effectue les écritures
  EEPROM.end();              // fin de session
}

// fonctions température
void VerifConnexionTMP102() // vérifie la connexion à TMP102
{
  // vérifie la connexion au capteur
  if (CaptTemp.begin() == false)
  {
    Serial.println("TMP102 non connecté !");
    while (1)
      ;
  }
  Serial.println("TMP102 connecté !");
}

int LireTemp() // lit et affiche la température
{
  int valRetour = 0;
  float temp = 0;
  // lecture de la température
  temp = CaptTemp.readTempC();
  // affichage de la température
  Serial.print("Temperature :\t");
  Serial.print(temp);
  Serial.println("°C");
  valRetour = temp * 100;

  return valRetour;
}

// fonctions ESP8266
void setup()
{
  pinMode(INTEAU, INPUT);  // configuration de l'interrupteur d'ouverture
  pinMode(INTMES, INPUT);  // configuration de l'interrupteur de mise en service
  pinMode(LED, OUTPUT);    // configuration de la led de mise en service
  Serial.begin(9600);      // configuration ESP8266
  Wire.begin();            // configuration de la liaison I2C
  VerifConnexionTMP102();  // vérification de la connexion à TMP102
  VerifConnexionMMA8452(); // vérification de la connexion à MMA8452
}

void loop()
{
  // initialisation des variables locales
  int vectTrame[TAILLE_TRAME] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
  float vectMesure[TAILLE_XYZ] = {0, 0, 0};
  float vectEEPROM[TAILLE_XYZ] = {0, 0, 0};
  EnleverMES();
  // détection des boutons
  if (digitalRead(INTMES) == HIGH) // si le bouton de mise en service est actionné
  {
    if (VerifMisEnService() == false) // si la mise en service n'a pas été faite
    {
      // message et témoin lumineux
      Serial.println("Mise en service de l'hydrant...");
      MiseEnService();
      digitalWrite(LED, HIGH);
      delay(1000);
      digitalWrite(LED, LOW);
      if (VerifMisEnService() == true)
      {
        Serial.println("Succès !");
      }
      else
      {
        Serial.println("Echec !");
      }
    }
    else // sinon
    {
      Serial.println("Hydrant déjà mis en service."); // message
    }
  }
  else // si le bouton de mise en service n'est pas actionné
  {
    if (digitalRead(INTEAU) == HIGH) // si le bouton d'ouverture est actionné
    {
      // message et ajout de l'état dans la trame
      Serial.println("Hydrant ouvert.");
      vectTrame[0] = 1;
    }
    else // sinon
    {
      // message et ajout de l'état dans la trame
      Serial.println("Hydrant fermé.");
      vectTrame[0] = 0;
    }
  }

  // accéléromètre
  LireXYZEEPROM(vectEEPROM);
  MesurerXYZ(vectMesure);
  vectTrame[1] = VerifInclinaison(vectMesure, vectEEPROM);
  // température
  vectTrame[2] = LireTemp();
  // affichage
  Serial.println("-------------------------------------");
  // veille
  ESP.deepSleep(VEILLE);
}