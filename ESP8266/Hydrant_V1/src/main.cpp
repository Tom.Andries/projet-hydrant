#include <Arduino.h>
#include <Wire.h>
#include <EEPROM.h>
#include <math.h>
#include "SparkFun_MMA8452Q.h"
#include "SparkFunTMP102.h"

// veille de 10 secondes
#define VEILLE 1E7
// tailles vecteurs
#define TAILLE_TRAME 10
#define TAILLE_XYZ 3
// adresses
#define ADRMES 0
#define ADROUV 2
#define ADRX 10
#define ADRY 14
#define ADRZ 18

// Branchements TMP102
// VCC = 3.3V
// GND = GND
// SDA = D2
// SCL = D1

// Branchements MMA8452
// 3.3V = 3.3V
// GND = GND
// SDA = D2
// SCL = D1

// objets des classes des capteurs
TMP102 CaptTemp;
MMA8452Q Accel;

// fonctions ouverture/fermeture
bool VerifOuverture()
{
  bool etatOuvert = false;

  EEPROM.begin(1);                             // début session EEPROM
  etatOuvert = EEPROM.get(ADROUV, etatOuvert); // prend l'état enregistré dans l'EEPROM

  if (etatOuvert == true) // si l'hydrant est ouvert
  {
    Serial.println("Hydrant Ouvert."); // message
    etatOuvert = false;                // passe à l'état "fermé"
    EEPROM.put(ADROUV, etatOuvert);    // demande d'écriture dans l'EEPROM
  }
  else
  {
    Serial.println("Hydrant Fermé."); // message
    etatOuvert = true;                // passe à l'état "ouvert"
    EEPROM.put(ADROUV, etatOuvert);   // demande d'écriture dans l'EEPROM
  }

  EEPROM.commit(); // effectue l'écriture
  EEPROM.end();    // fin de session

  return etatOuvert;
}

// fonctions accéléromètre
void VerifConnexionMMA8452()
{
  if (Accel.begin() == false)
  {
    Serial.println("MMA8452 non connecté !");
    while (1)
      ;
  }
  Serial.println("MMA8452 connecté !");
}

void EcrireXYZEEPROM() // écriture de X, Y et Z dans l'EEPROM
{
  EEPROM.begin(12); // début session EEPROM
  // mesures de X, Y et Z
  float vectMesure[3] = {Accel.getCalculatedX(), Accel.getCalculatedY(), Accel.getCalculatedZ()};
  // écriture dans l'EEPROM
  EEPROM.put(ADRX, vectMesure[0]);
  EEPROM.put(ADRY, vectMesure[1]);
  EEPROM.put(ADRZ, vectMesure[2]);
  EEPROM.commit();
  EEPROM.end(); // fin de session
}

void LireXYZEEPROM(float prmVectLecture[]) // retourne X, Y et Z dans l'EEPROM dans un vecteur
{
  EEPROM.begin(12); // début session EEPROM
  // lecture de X, Y et Z dans l'EEPROM
  EEPROM.get(ADRX, prmVectLecture[0]);
  EEPROM.get(ADRY, prmVectLecture[1]);
  EEPROM.get(ADRZ, prmVectLecture[2]);
  EEPROM.end(); // fin de session
}

void MesurerXYZ(float prmVectMesure[]) // mesure X, Y et Z dans un vecteur
{
  if (Accel.available()) // s'il y a une nouvelle mesure
  { 
    // écriture de X, Y et Z dans le vecteur
    prmVectMesure[0] = Accel.getCalculatedX();
    prmVectMesure[1] = Accel.getCalculatedY();
    prmVectMesure[2] = Accel.getCalculatedZ();
  }
}

bool VerifInclinaison(float prmVectMesure[], float prmVectEEPROM[]) // détecte une inclinaison
{
  bool etatInclinaison = false;
  float valXEEPROM = prmVectEEPROM[0],
        valYEEPROM = prmVectEEPROM[1],
        valXMesure = prmVectMesure[0],
        valYMesure = prmVectMesure[1],
        valZMesure = prmVectMesure[2],
        XY = 0,
        angle = 0;
  // calcul de l'angle
  XY = pow((valXMesure - valXEEPROM), 2);
  XY += pow((valYMesure - valYEEPROM), 2);
  XY = sqrt(XY) / valZMesure;
  angle = atan(XY);
  angle = (angle * 180) / PI;
  // affichage
  Serial.print("Angle :\t\t");
  Serial.print(angle, 2);
  Serial.println("°");
  // vérifie si l'hydrant est renversé
  if (angle > 8)
  {
    Serial.println("Hydrant renversé !");
    etatInclinaison = true;
  }

  return etatInclinaison;
}

// fonctions mise en service
bool VerifMisEnService()
{
  bool etatMES = 0;
  EEPROM.begin(1);                            // début session EEPROM
  EEPROM.get(ADRMES, etatMES); // lit la mis en service
  EEPROM.end();                               // fin de session EEPROM
  return etatMES;
}

void MiseEnService()
{
  EcrireXYZEEPROM();         // écriture de X, Y et Z dans l'EEPROM
  EEPROM.begin(1);           // début de session EEPROM
  EEPROM.put(ADROUV, false); // écriture de l'état "hydrant fermé"
  EEPROM.put(ADRMES, true);  // écriture de l'état "mis en service"
  EEPROM.commit();           // effectue les écritures
  EEPROM.end();              // fin de session
}

// fonctions température
void VerifConnexionTMP102() // vérifie la connexion à TMP102
{
  // vérifie la connexion au capteur
  if (CaptTemp.begin() == false)
  {
    Serial.println("TMP102 non connecté !");
    while (1)
      ;
  }
  Serial.println("TMP102 connecté !");
}

int LireTemp() // lit et affiche la température
{
  int valRetour = 0;
  float temp = 0;
  // lecture de la température
  temp = CaptTemp.readTempC();
  // affichage de la température
  Serial.print("Temperature :\t");
  Serial.print(temp);
  Serial.println("°C");
  valRetour = temp * 10;

  return valRetour;
}

// fonctions ESP8266
void setup()
{
  Serial.begin(9600);      // configuration ESP8266
  Wire.begin();            // configuration de la liaison I2C
  VerifConnexionTMP102();  // vérification de la connexion à TMP102
  VerifConnexionMMA8452(); // vérification de la connexion à MMA8452
  if (VerifMisEnService() == false) {
    MiseEnService();
    Serial.println("Mise en service de l'hydrant");
  }
}

void loop()
{
  int vectTrame[TAILLE_TRAME] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
  float vectMesure[TAILLE_XYZ] = {0, 0, 0};
  float vectEEPROM[TAILLE_XYZ] = {0, 0, 0};
  // accéléromètre
  LireXYZEEPROM(vectEEPROM);
  MesurerXYZ(vectMesure);
  vectTrame[0] = VerifInclinaison(vectMesure, vectEEPROM);
  // température
  vectTrame[1] = LireTemp();
  // pause
  delay(3000);
}

/*
int vectTrame[TAILLE_TRAME] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
  float vectMesure[TAILLE_VECT] = {0, 0, 0};
  float vectEEPROM[TAILLE_VECT] = {0, 0, 0};
  // mise en service
  if (VerifMisEnService() == false)
  {
    MiseEnService();
    Serial.println("Mise en service de l'hydrant.");
  }
  else
  {
    Serial.println("Hydrant déjà mis en service.");
  }
    // accéléromètre
  LireXYZEEPROM(vectEEPROM);
  MesurerXYZ(vectMesure);
  vectTrame[0] = VerifInclinaison(vectMesure, vectEEPROM);
  // ouverture/fermeture 
    il faut détecter s'il est ouvert ou fermé
    il faut aussi retourner l'état dans la trame
    problème : change d'état à chaque fois
    il faut attribuer la fonction à un bouton précis
    donc une broche
  
  vectTrame[1] = VerifOuverture();
  // température
  //LireTemp();
  // mise en veille 1 minute
  //ESP.deepSleep(VEILLE);
*/