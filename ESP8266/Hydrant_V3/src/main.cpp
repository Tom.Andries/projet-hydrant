#include <Arduino.h>
#include <Wire.h>
#include <EEPROM.h>
#include "UtiHydrant.h"

// broches ESP8266
#define INTEAU 14
#define INTMES 12
#define LED 15
// veille de 1 minute
#define VEILLE 6E7
// tailles vecteurs
#define TAILLE_TRAME 10
#define TAILLE_XYZ 3


// Branchements TMP102
// VCC = 3.3V
// GND = GND
// SDA = D2
// SCL = D1

// Branchements MMA8452
// 3.3V = 3.3V
// GND = GND
// SDA = D2
// SCL = D1


// fonctions ESP8266
void setup()
{
  pinMode(INTEAU, INPUT);  // configuration de l'interrupteur d'ouverture
  pinMode(INTMES, INPUT);  // configuration de l'interrupteur de mise en service
  pinMode(LED, OUTPUT);    // configuration de la led de mise en service
  Serial.begin(9600);      // configuration ESP8266
  Wire.begin();            // configuration de la liaison I2C
  VerifConnexionTMP102();  // vérification de la connexion à TMP102
  VerifConnexionMMA8452(); // vérification de la connexion à MMA8452
}

void loop()
{
  // initialisation des variables locales
  int vectTrame[TAILLE_TRAME] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
  float vectMesure[TAILLE_XYZ] = {0, 0, 0};
  float vectEEPROM[TAILLE_XYZ] = {0, 0, 0};
  EnleverMES();
  // détection des boutons
  if (digitalRead(INTMES) == HIGH) // si le bouton de mise en service est actionné
  {
    if (VerifMisEnService() == false) // si la mise en service n'a pas été faite
    {
      // message et témoin lumineux
      Serial.println("Mise en service de l'hydrant...");
      MiseEnService();
      digitalWrite(LED, HIGH);
      delay(1000);
      digitalWrite(LED, LOW);
      if (VerifMisEnService() == true)
      {
        Serial.println("Succès !");
      }
      else
      {
        Serial.println("Echec !");
      }
    }
    else // sinon
    {
      Serial.println("Hydrant déjà mis en service."); // message
    }
  }
  else // si le bouton de mise en service n'est pas actionné
  {
    if (digitalRead(INTEAU) == HIGH) // si le bouton d'ouverture est actionné
    {
      // message et ajout de l'état dans la trame
      Serial.println("Hydrant ouvert.");
      vectTrame[0] = 1;
    }
    else // sinon
    {
      // message et ajout de l'état dans la trame
      Serial.println("Hydrant fermé.");
      vectTrame[0] = 0;
    }
  }

  // accéléromètre
  LireXYZEEPROM(vectEEPROM);
  MesurerXYZ(vectMesure);
  vectTrame[1] = VerifInclinaison(vectMesure, vectEEPROM);
  // température
  vectTrame[2] = LireTemp();
  // affichage
  Serial.println("-------------------------------------");
  // veille
  ESP.deepSleep(VEILLE);
}