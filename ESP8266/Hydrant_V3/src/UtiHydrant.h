//---------------------------------------------------------------------------
// UtilHydrant.h
// fichier d'interface des fonctions utilisées par l'hydrant
// Créé par Tom andries le 06/04/2020
//---------------------------------------------------------------------------

#ifndef UtiHydrantH
#define UtiHydrantH

#include <Arduino.h>
#include <Wire.h>
#include <EEPROM.h>
#include <math.h>
#include "SparkFunTMP102.h"
#include "SparkFun_MMA8452Q.h"

// adresses
#define ADRMES 0
#define ADRX 10
#define ADRY 14
#define ADRZ 18
//---------------------------------------------------------------------------

// objets des classes des capteurs
extern TMP102 CaptTemp;
extern MMA8452Q Accel;

// fonctions accéléromètre
void VerifConnexionMMA8452();
void EcrireXYZEEPROM();
void LireXYZEEPROM(float prmVectLecture[]);
void MesurerXYZ(float prmVectMesure[]);
bool VerifInclinaison(float prmVectMesure[], float prmVectEEPROM[]);

// fonctions mise en service
bool VerifMisEnService();
void MiseEnService();
void EnleverMES();

// fonctions température
void VerifConnexionTMP102();
int LireTemp();

#endif
 