#include <Arduino.h>
#include <SparkFunTMP102.h> // lib TMP102
#include <Wire.h> // lib I2C

// Branchements
// VCC = 3.3V
// GND = GND
// SDA = D2
// SCL = D1

TMP102 CaptTemp;

void VerifConnexion()                       // vérifie la connexion à TMP102
{
  // vérifie la connexion au capteur
  if (!CaptTemp.begin())
  {
    Serial.print("Cannot connect to TMP102.\n");
    Serial.print("Is the board connected? Is the device ID correct?");
    while (1);
  }
  Serial.print("Connected to TMP102!\n");
}

void AfficherTemp()                         // lit et affiche la température
{
  // lecture de la température
  float temperature = 0;
  temperature = CaptTemp.readTempC();
  // affichage de la température
  Serial.print("Temperature : ");
  Serial.print(temperature);
  Serial.print("°C");
  Serial.print("\n");
}

void setup()
{
  Serial.begin(9600);
  Wire.begin(); //configuration de la liaison I2C
  VerifConnexion();
  delay(100);
}

void loop()
{
  AfficherTemp();
  ESP.deepSleep(0);
}