#include <Arduino.h>
#include <SoftwareSerial.h>
#include <string>
#include "UtiHydrant.h"

// broches ESP8266
#define INTEAU 14
#define INTMES 12
#define LED 15
#define RX 0
#define TX 2
// veille de 1 minute
#define VEILLE 6E7
// tailles vecteurs
#define TAILLE_TRAME 6
#define TAILLE_XYZ 3
// mode pour mesurer la tension
ADC_MODE(ADC_VCC);
// utilisation de string
using namespace std;
// objet pour transmettre la trame
SoftwareSerial Modem(RX, TX);

// Branchements TMP102
// VCC = 3.3V
// GND = GND
// SDA = D2
// SCL = D1

// Branchements MMA8452
// 3.3V = 3.3V
// GND = GND
// SDA = D2
// SCL = D1

// Branchements Modem Sigfox
//
//
// RX = D3
// TX = D4

// fonctions ESP8266
void setup()
{
  pinMode(INTEAU, INPUT);  // configuration de l'interrupteur d'ouverture
  pinMode(INTMES, INPUT);  // configuration de l'interrupteur de mise en service
  pinMode(LED, OUTPUT);    // configuration de la led de mise en service
  Serial.begin(9600);      // configuration ESP8266
  Modem.begin(9600);       // configuration modem Sigfox
  Wire.begin();            // configuration de la liaison I2C
  VerifConnexionTMP102();  // vérification de la connexion à TMP102
  VerifConnexionMMA8452(); // vérification de la connexion à MMA8452
}

void loop()
{
  // initialisation des variables locales
  short temperature = 0;
  char vectTrame[TAILLE_TRAME];
  string trame = "";
  float vectMesure[TAILLE_XYZ] = {0, 0, 0};
  float vectEEPROM[TAILLE_XYZ] = {0, 0, 0};
  EnleverMES();
  // détection des boutons
  if (digitalRead(INTMES) == HIGH) // si le bouton de mise en service est actionné
  {
    vectTrame[0] = VerifMisEnService();
    if (VerifMisEnService() == false) // si la mise en service n'a pas été faite
    {
      // message
      Serial.println("Mise en service de l'hydrant...");
      MiseEnService();
      // envoi trame vide
      char vectTrameMES[1] = {0};
      string trameMES = EcrireTrame(vectTrameMES);
      Modem.write(trameMES.c_str());
      // témoin lumineux
      digitalWrite(LED, HIGH);
      delay(1000);
      digitalWrite(LED, LOW);
      // vérification de la mise en service
      if (VerifMisEnService() == true)
      {
        Serial.println("Succès !");
      }
      else
      {
        Serial.println("Echec !");
      }
    }
    else // sinon
    {
      Serial.println("Hydrant déjà mis en service."); // message
    }
  }
  else // si le bouton de mise en service n'est pas actionné
  {
    if (digitalRead(INTEAU) == HIGH) // si le bouton d'ouverture est actionné
    {
      // message et ajout de l'état dans la trame
      Serial.println("Hydrant ouvert.");
      vectTrame[1] = true;
    }
    else // sinon
    {
      // message et ajout de l'état dans la trame
      Serial.println("Hydrant fermé.");
      vectTrame[1] = false;
    }
  }
  // accéléromètre
  LireXYZEEPROM(vectEEPROM);
  MesurerXYZ(vectMesure);
  vectTrame[2] = VerifInclinaison(vectMesure, vectEEPROM);
  // température
  temperature = LireTemp();
  vectTrame[3] = temperature >> 8;     // prend l'octet de poid fort
  vectTrame[4] = temperature & 0x00FF; // prend l'octet de poid faible
  // batterie
  vectTrame[5] = LirePourcentBat();
  // trame
  trame = EcrireTrame(vectTrame);
  Modem.write(trame.c_str());
  // affichage
  Serial.println("-------------------------------------");
  // veille
  ESP.deepSleep(VEILLE);
}