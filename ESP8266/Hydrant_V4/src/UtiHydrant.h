//---------------------------------------------------------------------------
// UtilHydrant.h
// fichier d'interface des fonctions utilisées par l'hydrant
// Créé par Tom andries le 06/04/2020
//---------------------------------------------------------------------------

#ifndef UtiHydrantH
#define UtiHydrantH

#include <Arduino.h>
#include <Wire.h>
#include <EEPROM.h>
#include <math.h>
#include <string>
#include "SparkFunTMP102.h"
#include "SparkFun_MMA8452Q.h"

// adresses
#define ADRMES 0
#define ADRBAT 1
#define ADRX 10
#define ADRY 14
#define ADRZ 18
// tension minimale batterie
#define SEUIL 2.8
// objets des classes des capteurs
extern TMP102 CaptTemp;
extern MMA8452Q Accel;
// utilisation de string
using namespace std;

// fonction trame
string EcrireTrame(char prmVect []);

// fonctions batterie
void LireBat();
unsigned char LirePourcentBat();
void EcrireBatEEPROM();
float LireBatEEPROM();

// fonctions accéléromètre
void VerifConnexionMMA8452();
void EcrireXYZEEPROM();
void LireXYZEEPROM(float prmVectLecture[]);
void MesurerXYZ(float prmVectMesure[]);
bool VerifInclinaison(float prmVectMesure[], float prmVectEEPROM[]);

// fonctions mise en service
bool VerifMisEnService();
void MiseEnService();
void EnleverMES();

// fonctions température
void VerifConnexionTMP102();
short LireTemp();

#endif
 