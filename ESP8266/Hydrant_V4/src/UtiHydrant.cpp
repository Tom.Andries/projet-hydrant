//---------------------------------------------------------------------------
// UtilHydrant.cpp
// fichier d'implémentation des fonctions utilisées par l'hydrant
// Créé par Tom andries le 06/04/2020
//---------------------------------------------------------------------------

#include "UtiHydrant.h"

// objets des classes des capteurs
TMP102 CaptTemp;
MMA8452Q Accel;

// fonction trame
string EcrireTrame(char prmVect[])
{
  int quartetH = 0,
      quartetB = 0;
  string trame = "AT$RAW=";
  // boucle pour chaque octet
  for (int i = 0; i < 6; i++)
  {
    quartetH = prmVect[i] & 0xF0; // prend l'octet de poid fort
    quartetH = quartetH >> 4;     // décalage
    quartetB = prmVect[i] & 0x0F; // prend l'octet de poid faible
    // conversion de quartetH
    if (quartetH < 10)
    {                          // si c'est un chiffre
      trame += '0' + quartetH; // ajout du code ASCCI
    }
    else
    {                                   // si c'est une lettre
      trame += (quartetH - 0x0A) + 'A'; // ajout du code ASCII
    }
    // conversion de quartetB
    if (quartetB < 10)
    {                          // si c'est un chiffre
      trame += '0' + quartetB; // ajout du code ASCII
    }
    else
    {                                   // si c'est une lettre
      trame += (quartetB - 0x0A) + 'A'; // ajout du code ASCII
    }
  }
  trame += '\r';
  trame += '\n';
  return trame;
}

// fonctions batterie
unsigned char LirePourcentBat()
{
  float tensionMax = 0,
        tensionMesure = 0;
  unsigned char tension = 0;
  tensionMax = LireBatEEPROM()/1024.00;
  tensionMesure = ESP.getVcc()/1024.00;
  tension = ((tensionMesure - SEUIL) * 100) / (tensionMax - SEUIL);
  // affichage
  Serial.print("Batterie :\t");
  Serial.print(tension);
  Serial.println("%");

  return tension;
}

void EcrireBatEEPROM()
{ // écrit la tension dans l'EEPROM
  short tension = 0;
  tension = ESP.getVcc();
  EEPROM.begin(2);             // début session EEPROM
  EEPROM.put(ADRBAT, tension); // écriture dans l'EEPROM
  EEPROM.commit();
  EEPROM.end(); // fin de session
}

float LireBatEEPROM()
{ // lit la tension enregistrée dans l'EEPROM
  short tension = 0;
  EEPROM.begin(2);             // début session EEPROM
  EEPROM.get(ADRBAT, tension); // lecture de la batterie enregistrée dans l'EEPROM
  EEPROM.end();                // fin de session

  return tension;
}

// fonctions accéléromètre
void VerifConnexionMMA8452()
{
  if (Accel.begin() == false)
  {
    Serial.println("MMA8452 non connecté !");
    while (1)
      ;
  }
  Serial.println("MMA8452 connecté !");
}

void EcrireXYZEEPROM() // écriture de X, Y et Z dans l'EEPROM
{
  // mesures de X, Y et Z
  float vectMesure[3] = {Accel.getCalculatedX(), Accel.getCalculatedY(), Accel.getCalculatedZ()};
  // écriture dans l'EEPROM
  EEPROM.begin(12); // début session EEPROM
  EEPROM.put(ADRX, vectMesure[0]);
  EEPROM.put(ADRY, vectMesure[1]);
  EEPROM.put(ADRZ, vectMesure[2]);
  EEPROM.commit();
  EEPROM.end(); // fin de session
}

void LireXYZEEPROM(float prmVectLecture[]) // retourne X, Y et Z dans l'EEPROM dans un vecteur
{
  EEPROM.begin(12); // début session EEPROM
  // lecture de X, Y et Z dans l'EEPROM
  EEPROM.get(ADRX, prmVectLecture[0]);
  EEPROM.get(ADRY, prmVectLecture[1]);
  EEPROM.get(ADRZ, prmVectLecture[2]);
  EEPROM.end(); // fin de session
}

void MesurerXYZ(float prmVectMesure[]) // mesure X, Y et Z dans un vecteur
{
  if (Accel.available()) // s'il y a une nouvelle mesure
  {
    // écriture de X, Y et Z dans le vecteur
    prmVectMesure[0] = Accel.getCalculatedX();
    prmVectMesure[1] = Accel.getCalculatedY();
    prmVectMesure[2] = Accel.getCalculatedZ();
  }
}

bool VerifInclinaison(float prmVectMesure[], float prmVectEEPROM[]) // détecte une inclinaison
{
  bool etatInclinaison = false;
  float valXEEPROM = prmVectEEPROM[0],
        valYEEPROM = prmVectEEPROM[1],
        valXMesure = prmVectMesure[0],
        valYMesure = prmVectMesure[1],
        valZMesure = prmVectMesure[2],
        XY = 0,
        angle = 0;
  // calcul de l'angle
  XY = pow((valXMesure - valXEEPROM), 2);
  XY += pow((valYMesure - valYEEPROM), 2);
  XY = sqrt(XY) / valZMesure;
  angle = atan(XY);
  angle = (angle * 180) / PI;
  // affichage
  Serial.print("Angle :\t\t");
  Serial.print(angle, 2);
  Serial.println("°");
  // vérifie si l'hydrant est renversé
  if (angle > 8)
  {
    Serial.println("Hydrant renversé !");
    etatInclinaison = true;
  }

  return etatInclinaison;
}

// fonctions mise en service
bool VerifMisEnService()
{
  bool etatMES = 0;
  EEPROM.begin(1);             // début session EEPROM
  EEPROM.get(ADRMES, etatMES); // lit la mis en service
  EEPROM.end();                // fin de session EEPROM
  return etatMES;
}

void MiseEnService()
{
  EcrireXYZEEPROM();
  EcrireBatEEPROM();
  EEPROM.begin(1);          // début de session EEPROM
  EEPROM.put(ADRMES, true); // écriture de l'état "mis en service"
  EEPROM.commit();          // effectue les écritures
  EEPROM.end();             // fin de session
}

void EnleverMES()
{
  EEPROM.begin(1);           // début de session EEPROM
  EEPROM.put(ADRMES, false); // écriture de l'état "mis en service"
  EEPROM.commit();           // effectue les écritures
  EEPROM.end();              // fin de session
}

// fonctions température
void VerifConnexionTMP102() // vérifie la connexion à TMP102
{
  // vérifie la connexion au capteur
  if (CaptTemp.begin() == false)
  {
    Serial.println("TMP102 non connecté !");
    while (1)
      ;
  }
  Serial.println("TMP102 connecté !");
}

short LireTemp() // lit et affiche la température
{
  short valRetour = 0;
  float temp = 0;
  // lecture de la température
  temp = CaptTemp.readTempC();
  // affichage de la température
  Serial.print("Temperature :\t");
  Serial.print(temp);
  Serial.println("°C");
  valRetour = temp * 100;

  return valRetour;
}
