#include <Arduino.h>
#include <EEPROM.h>

#define ADRESSE 2
#define PAUSE 5000

void setup()
{
  Serial.begin(9600);
  EEPROM.begin(1);
}

void loop()
{
  bool etatOuvert = EEPROM.get(ADRESSE, etatOuvert);
  if (etatOuvert == true) {
    Serial.println("Hydrant Ouvert");
    etatOuvert = false;
    EEPROM.put(ADRESSE, etatOuvert);
  } else {
    Serial.println("Hydrant Fermé");
    etatOuvert = true;
    EEPROM.put(ADRESSE, etatOuvert);
  }
  EEPROM.commit();
  ESP.deepSleep(0);
}


/*  Code qui fonctionne
bool etatOuvert = false;
  EEPROM.put(ADRESSE, etatOuvert);
  EEPROM.commit();
  bool test = EEPROM.get(ADRESSE, etatOuvert);
  Serial.print("\nValeur du test : ");
  Serial.print(test);
  delay(PAUSE);
*/