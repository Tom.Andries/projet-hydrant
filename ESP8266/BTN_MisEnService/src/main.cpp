#include <Arduino.h>
#include <EEPROM.h>

#define ADRESSE 2

void LireEtatMES() {
  EEPROM.begin(1);
  bool etat = EEPROM.read(ADRESSE);
  Serial.print("Valeur de l'état : ");
  Serial.print(etat);
  Serial.print("\n");
  EEPROM.end();
}

void ForcerMisEnService()
{
  EEPROM.begin(1);
  EEPROM.write(ADRESSE, true);
  EEPROM.commit();
  EEPROM.end();
  Serial.print("Forçage de la mise en service\n");
}

void AnnulerMisEnService()
{
  EEPROM.begin(1);
  EEPROM.write(ADRESSE, false);
  EEPROM.commit();
  EEPROM.end();
  //Serial.print("Annulation de la mise en service\n");
}

void VerifMisEnService()
{
  EEPROM.begin(1); // début session EEPROM

  if (EEPROM.read(ADRESSE) == true) // si l'hydrant est déjà mis en service
  {
    Serial.print("Hydrant déjà mis en service\n"); // message
  }
  else // sinon
  {
    Serial.print("Mise en service de l'hydrant\n"); // message
    EEPROM.write(ADRESSE, true);                    // demande de changement l'état de la mise en service
    EEPROM.commit();                                // effectue l'écriture
  }

  EEPROM.end(); // fin de session EEPROM
}

void setup()
{
  Serial.begin(9600);

  AnnulerMisEnService();
  VerifMisEnService();
  VerifMisEnService();
  VerifMisEnService();
  VerifMisEnService();
}

void loop()
{
  ESP.deepSleep(0);
}