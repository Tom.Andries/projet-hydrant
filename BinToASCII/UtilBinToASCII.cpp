//---------------------------------------------------------------------------
// Fichier "UtilBinToASCII.cpp"
// Fichier d'impl�mentation de la fonction BinToASCII
// Cr�� par Tom Andries le 30/04/2021
//---------------------------------------------------------------------------


#pragma hdrstop
#include "UtilBinToASCII.h"

//---------------------------------------------------------------------------
string BinToASCII(char prmVect[]) {
  string valRetour  = "";
  int quartetH = 0,
      quartetB = 0;

  // boucle pour chaque octet
  for (int i = 0; i < 6; i++) {
    quartetH = prmVect[i] & 0xF0;               // prend l'octet de poid fort
    quartetH = quartetH >> 4;                   // d�calage
    quartetB = prmVect[i] & 0x0F;               // prend l'octet de poid faible
    // conversion de quartetH
    if (quartetH < 10) {                        // si c'est un chiffre
      valRetour += '0' + quartetH;              // ajout du code ASCCI
    } else {                                    // si c'est une lettre
      valRetour += (quartetH - 0x0A) + 'A';     // ajout du code ASCII
    }
    // conversion de quartetB
    if (quartetB < 10) {                        // si c'est un chiffre
      valRetour += '0' + quartetB;              // ajout du code ASCII
    } else {                                    // si c'est une lettre
      valRetour += (quartetB - 0x0A) + 'A';     // ajout du code ASCII
    }
  }
  return valRetour;                             // retourne la trame
}

#pragma package(smart_init)
 