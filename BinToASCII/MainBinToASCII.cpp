//---------------------------------------------------------------------------

#include <vcl.h>
#include "UtilBinToASCII.h"
#pragma hdrstop

//---------------------------------------------------------------------------

#pragma argsused
int main(int argc, char* argv[])
{
  char vectData[6] = {16,255,10,1,0,127};
  string trame = "";
  trame = "AT$RAW=";
  trame += BinToASCII(vectData);
  trame += '\r';
  trame += '\n';

        return 0;
}
//---------------------------------------------------------------------------
