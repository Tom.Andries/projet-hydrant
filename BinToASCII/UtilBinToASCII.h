//---------------------------------------------------------------------------
// Fichier "UtilBinToASCII.h"
// Fichier d'interface de la fonction BinToASCII
// Cr�� par Tom Andries le 30/04/2021
//---------------------------------------------------------------------------

#ifndef UtilBinToASCIIH
#define UtilBinToASCIIH

#include <string>

//---------------------------------------------------------------------------

using namespace std;
string BinToASCII(char prmVect[]);


#endif
