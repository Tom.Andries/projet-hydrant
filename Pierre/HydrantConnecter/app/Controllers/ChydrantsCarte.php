<?php
namespace App\Controllers;
use CodeIgniter\Controller;
use App\Models\Mhydrants;
use \CodeIgniter\Exceptions\PageNotFoundException;

class ChydrantsCarte extends Controller
{
	public function index()
	{
		$session = session();
		if($session->get('idContact')===null){      //Session : permet de renvoyer l'utilisateur à la page de connexion s'il n'est pas connecté
			return redirect()->to('/login');
		}
		echo "Bienvenue, Mr " . $session->get('nom');  //Afficher le nom de l'utilisateur sur les pages

		$data['title']   = "Les Hydrants";   
		$data['result']   =  [];

		$model = new Mhydrants();  //Appel du model pour les requetes de la bdd
		$hydrants = $model->getAll();  //récup tout les hydrants (données de la bdd)

		foreach ($hydrants as $hydrant) {  
			array_push($data['result'], $model->getDetail($hydrant['idHydrant']));
		}

		$page['contenu'] = view('Hydrants/v_hydrant_carte', $data);    //Appel de view hydrant carte

		return view('Commun/v_templateConnecter', $page);    //return du template connecter
	}

	public function detail($prmId = null)                        
	{
		$session = session();
		if($session->get('idContact')===null){		//Session qui permet de renvoyer l'utilisateur à la page de connexion s'il n'est pas connecté
			return redirect()->to('/login');
		}
		
		if ($prmId != null) {
		echo "Bienvenue, Mr " . $session->get('nom');
		$data['title']   = "Les Hydrants";
		$modelHydrants = new Mhydrants();
		$data['result'] = $modelHydrants->getDetail($prmId);


		if (count($data['result']) != 0) {
			$page['contenu'] = view('Hydrants/v_detail_hydrants', $data);  //Appel de view hydrant carte détailler pour 1 hydrant
			return view('Commun/v_templateConnecterDetail', $page); //return du template connecter détail
		} else {
			throw PageNotFoundException::forPageNotFound("Cette hydrant n'existe pas !");
		}
	}else{
		throw PageNotFoundException::forPageNotFound("Il faut choisir un hydrant valide ou qui possède des données !");
	}
	}

}

