<?php

namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\Muser;

class Login extends Controller
{
    public function index()
    {
        helper(['form']);
        $page['contenu'] = view('Hydrants/v_login');

        return view('Commun/v_template', $page);
    }

    public function auth()
    {
        helper(['form']);
        $session = session();
        $model = new Muser();
        $mail = $this->request->getVar('mail');
        $password = $this->request->getVar('pass');
        $data = $model->where('mail', $mail)->first();
        if ($data) {
            $pass = $data['pass'];

            $verify_pass = password_verify($password, $pass);
            if ($verify_pass == true) {

                $ses_data = [
                    'idContact'       => $data['idContact'],
                    'nom'     => $data['nom'],
                    'mail'    => $data['mail'],
                    'logged_in'     => TRUE
                ];
                $session->set($ses_data);
                return redirect()->to('/ChydrantsCarte');
            } else {
                $session->setFlashdata('msg', 'Mauvais mot de passe');
                echo view('Hydrants/v_login');
            }
        } else {
            $session->setFlashdata('msg', 'Email non valide');
            echo view('Hydrants/v_login');
        }
    }

    public function logout()
    {
        $session = session();
        $session->destroy();
        return redirect()->to('/login');
    }
}
