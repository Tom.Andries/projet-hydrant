<?php

namespace App\Controllers;


use CodeIgniter\Controller;

class CajouterHydrant extends Controller
{
	public function index()
	{
		$session = session();
		if($session->get('idContact')===null){
			return redirect()->to('/login');
		}
		echo "Bienvenue, Mr " . $session->get('nom');

		$page['contenu'] = view('Hydrants/v_ajout_hydrant');
		return view('Commun/v_templateConnecterDetail', $page);
		return view('Commun/v_templateConnecter', $page);
	}
}
