<?php

namespace App\Controllers;

use App\Models\Mhydrants;
use CodeIgniter\Controller;

class Chydrants extends Controller
{
	public function index()
	{
		$data['title'] = "Les Hydrants";
		$data['Heading'] = "MonHeading";

		$model = new Mhydrants();
		$data['result'] = $model->getAllWithPager();
		$data['pager'] = $model->pager;

		$page['contenu'] = view('Hydrants/v_detail_hydrants', $data);
		return view('Commun/v_template', $page);
	}

	public function detail($prmId = null)
	{
		//$prmId vaudra 15 dans cet exemple d’URL 
		// si pas de 3ème segment d’URL, alors $prmId = null
		$prmIdMax = 50;                      // Nombre de conteneurs
		if ($prmId <= $prmIdMax && $prmId > 0) {
			$model = new Mhydrants();
			$data['result'] = $model->getDetail($prmId);

			$page['contenu'] = view('Hydrants/v_detail_hydrants', $data);
			return view('Commun/v_template', $page);
		}
	}
}
