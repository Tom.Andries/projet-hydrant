<?php

namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\Muser;

class Cuser extends Controller
{
    public function index()
    {
        //include helper form
        helper(['form']);
        $data = [];
        echo view('Hydrants/v_connection_hydrants', $data);
    }

    public function save()
    {
        //include helper form
        helper(['form']);
        //set rules validation form
        $rules = [
            'nom'          => 'required|min_length[3]|max_length[20]',
            'mail'         => 'required|min_length[6]|max_length[50]',
            'pass'      => 'required|min_length[6]|max_length[200]',
            'confpass'  => 'matches[pass]'
        ];

        if ($this->validate($rules)) {
            $model = new Muser();
            $data = [
                'nom'     => $this->request->getVar('nom'),
                'mail'    => $this->request->getVar('mail'),
                'pass' => password_hash($this->request->getVar('pass'), PASSWORD_DEFAULT)
            ];
            $model->save($data);
            echo view('Hydrants/v_login');
        } else {
            $data['validation'] = $this->validator;
            echo view('Hydrants/v_connection_hydrants', $data);
        }
    }
}
