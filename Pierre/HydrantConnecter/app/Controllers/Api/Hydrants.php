<?php

namespace App\Controllers\Api;

use CodeIgniter\RESTful\ResourceController;
use CodeIgniter\API\ResponseTrait;

class Hydrants extends ResourceController
{
    protected $modelName = 'App\Models\Mhydrants';
    protected $model;

    protected $format = 'json';

    public function index($id = null)
    {
        $requete = $this->model->getAll();
        return $this->respond($requete, 200);
    }

    public function show($prmId = null)
    {
        $requete = $this->model->getAllByidhydrants($prmId);
        return $this->respond($requete, 200);
    }

    // Parti pour l'ajax modifier tableau
    public function initTableau($prmId = null)
    {
        $requete = $this->model->getInitAjax();
        return $this->respond($requete, 200);
    }
    
    public function majTableau($prmDateDernierMaj)
    {
            $retour = $this->model->getMajAjax($prmDateDernierMaj);
            return $this->respond($retour, 201);
    }

    //Parti calcul volume d'eau
    public function SommeByIdHydrants($prmId)
    {
        $date = $this->request->getPost('date');
        $date = json_decode($date, true);
        if ($date != "") {
            $retour = $this->model->getSommeByIdHydrants($prmId,$date);
            return $this->respond($retour, 201);
        } else {
            return $this->respond("", 400);
        }
    }







 // Gaulthier
    public function create()
    {
        $data = $this->request->getPost('dto');
        $data = json_decode($data, true);
        if ($data != "") {
            $retour = $this->model->AddHydrant($data);
            return $this->respond($retour, 201);
        } else {
            return $this->respond("", 400);
        }
    }

    public function installOK()
    {

        $requete = $this->model->getAllByInstallOK();
        return $this->respond($requete, 200);
    }

}
