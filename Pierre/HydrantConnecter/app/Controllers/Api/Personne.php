<?php

namespace App\Controllers\Api;

use CodeIgniter\RESTful\ResourceController;
use CodeIgniter\API\ResponseTrait;

class Personne extends ResourceController
{
    protected $modelName = 'App\Models\Mpersonne';
    protected $model;

    protected $format = 'json';


    public function index()
    {

        //Recuperation des données 
        $data = $this->request->getGet('dto');
        $data = json_decode($data, true);
        
        if (!empty($data)) {
            //Appel de la fonction  de connexion
            return $this->model->Connexion($data);
            //return json_encode($data);
        }
    }
}
