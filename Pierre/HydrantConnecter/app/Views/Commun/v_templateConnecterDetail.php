<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- <title>Les hydrants connectés</title> -->
    <link href="<?= base_url("css/conteneurs.css") ?>" rel="stylesheet" type="text/css" />
    <script src="<?= base_url("js/jquery/jquery.js") ?>"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" />
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" ></script>
</head>

<body>
    <div id="conteneur">
        <header>
            <!-- <h1>Les hydrants connectés </h1> -->
        </header>

        <nav>
            <ul>
                <?php $ctrl = current_url(true)->getSegment(1);
                if ($ctrl == "") $ctrl = "Caccueil"; ?>

                <li <?php if ($ctrl == "ChydrantsCarte") {
                        echo 'class="active"';
                    } ?>><a href="<?= base_url("ChydrantsCarte/index") ?>">Carte des hydrants</a></>
                <li <?php if ($ctrl == "CajouterHydrant") {
                        echo 'class="active"';
                    } ?>><a href="<?= base_url("CajouterHydrant/index") ?>">Ajouter des hydrants</a></li>
                <li <?php if ($ctrl == "login") {
                        echo 'class="active"';
                    } ?>><a href="<?= base_url("login/logout") ?>">Déconnexion</a></li>

            </ul>
        </nav>
        <section>
            <?= $contenu ?>
        </section>

        <footer>
            <p>| Partenaires | Crédits & mentions légales |</p>
        </footer>
    </div>
</body>
</html>