<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- <title>Les hydrants connectés</title> -->
    <link href="<?= base_url("css/conteneurs.css") ?>" rel="stylesheet" type="text/css" />
</head>

<body>
    <div id="conteneur">
        <header>
            <!-- <h1>Les hydrants connectés </h1> -->
        </header>

        <nav>
            <ul>
                <?php $ctrl = current_url(true)->getSegment(1);
                if ($ctrl == "") $ctrl = "Caccueil"; ?>

                <li <?php if ($ctrl == "login") {
                        echo 'class="active"';
                    } ?>><a href="<?= base_url("login/index") ?>">Authentification</a></li>
                <li <?php if ($ctrl == "Caccueil") {
                        echo 'class="active"';
                    } ?>><a href="<?= base_url("Caccueil/index") ?>">Accueil</a></li>

            </ul>
        </nav>
        <section>
            <?= $contenu ?>
        </section>

        <footer>
            <p>| Partenaires | Crédits & mentions légales |</p>
        </footer>
    </div>
</body>
</html>