<div id="form-insert">
    <h3>Ajouter un nouvelle hydrant</h3>
    <h5> information sur l'hydrant : </h5>
    <div class="caption">Adresse : </div>
    <div class="input">
        <input id="adresse" maxlengh="100" name="adresse" type="text" value="" placeholder="Adresse">

    </div></br>

    <div class="caption">Latitude : </div>
    <div class="input">
        <input id="lat" maxlengh="100" name="latitude" type="text" value="" placeholder="Latitude">

    </div></br>

    <div class="caption">Longitude : </div>
    <div class="input">
        <input id="long" maxlengh="100" name="longitude" type="text" value="" placeholder="Longitude">

    </div></br>

    <div class="caption">Débit Nominal :</div>
    <div class="input">
        <input id="debitNominal" maxlengh="100" name="debit" type="text" value="" placeholder="Débit Nominal">

    </div></br>

    <br>

    <div class="button">
        <input type="submit" value="Ajouter l'hydrant" id="btnAjouter">
    </div>
    <script src="<?= base_url("js/jquery/jquery.js") ?> "></script>
    <script src="<?= base_url("js/ajouterHydrant.js") ?> "></script>