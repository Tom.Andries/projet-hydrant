<!-- Inclusion de la bibliothèque Leaflet et sa feuille de style. -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/leaflet.css" />

<!-- Le conteneur de notre carte (avec une contrainte CSS pour la taille -->
<div id="macarte" style="width:100%; height:600px"></div>

<article><br>

<center>
<h5>Calculer le volume d'eau puisée sur une durée de : </h5>
    <form>
      <select name="durée">
        <option selected disabled> -- Select -- </option>
        <option value='jours'> 7 jours</option>
        <option value='mois'> 1 mois</option>
        <option value='sixmois'> 6 mois</option>
      </select>
      <input type="submit" name="submit" value="Selectionner la durée" />
    </form>
    <?php 
      if (isset($_POST['submit']))
        {
            $getdurée=$_POST['durée'];
            echo ' Sur'.$getdurée.'test';
        }
    ?> 
  </center>

  <!-- Tableau des hydrants -->
  <table id="tableDetail">
    <thead>
      <tr>
        <th>Numéro Sigfox</th>
        <th>Date d'installation</th>
        <th>Adresse</th>
        <th>Etat de la borne</th>
        <th>Niveau de batterie en %</th>
        <th>Etat de veille </th>
        <th>Etat de renversement</th>
        <th>Température en °C</th>
        <th>Nombre d'ouverture</th>
        <th>Plus d'informations</th>
      </tr>
    </thead>
    <tbody>

      <?php foreach ($result as $hydrant) { ?>
        <?php $nbOuverture = 0; // Variable pour compter le nombre d'ouverture sur l'hydrant
        $EtatRenversement = "Non renverser";

        ?>
        <!-- Compter le nombre de typeMess = ouverture pour avoir le nombre d'ouverture -->
        <?php foreach ($hydrant as $data) { ?>
          <?php if ($data['typeMess'] === 'ouverture') {
            $nbOuverture++;
          } ?>

          <!-- Si l'état de l'hydrant est 0 mettre non renverser si non mettre hydrant renverser -->
          <?php if ($data['renversé'] === '0') {
            $EtatRenversement = "Non renversé";
          } else {
            $EtatRenversement = "Hydrant renversé";
          } ?>

        <?php } ?>
        <tr data-id='<?= $hydrant[0]['idHydrant'] ?>'>
          <td><?= $hydrant[0]['codeSigfox'] ?></td>
          <td><?= $hydrant[0]['dateFirstActivation'] ?></td>
          <td><?= $hydrant[0]['adresse'] ?></td>
          <td class="typeMess"><?= $hydrant[0]['typeMess'] ?></td>
          <td class="batterie"><?= $hydrant[0]['Batterie'] ?></td>
          <td class="actifVeille"><?= $hydrant[0]['actifVeille'] ?></td>
          <td class="etatRenversement"><?= $EtatRenversement ?></td>
          <td class="temperature"><?= $hydrant[0]['temperature'] ?></td>
          <td><?= $nbOuverture ?></td>
          <td><a href="<?= base_url("ChydrantsCarte/detail/" . $hydrant[0]['idHydrant']) ?>">Plus d'information</a></td>
        </tr>
      <?php } ?>
    </tbody>
  </table>

</article>


<script src="https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/leaflet.js"></script>
<script src="<?= base_url("js/main.js") ?>"></script>
<script src="<?= base_url("js/gestionTableau.js") ?>"></script>