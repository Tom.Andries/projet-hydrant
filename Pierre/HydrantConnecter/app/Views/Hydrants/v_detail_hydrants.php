<!-- Inclusion de la bibliothèque Leaflet et sa feuille de style. -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/leaflet.css" />

<!-- Le conteneur de notre carte (avec une contrainte CSS pour la taille -->
<div id="macarte" style="width:100%; height:600px"></div>

<article>
  <p>Le code Sigfox de cet hydrant est : <?= $result[0]['codeSigfox'] ?> <br>
    La date d'installation est : <?= $result[0]['dateFirstActivation'] ?> <br>
    Il est situer au <?= $result[0]['adresse'] ?>
  </p>
  <!-- Tableau des hydrants -->
  <table id="tableDetail">
    <thead>
      <tr>
        <th>Date</th>
        <th>Etat d'ouverture de la borne</th>
        <th>Niveau de batterie en %</th>
        <th>Etat de veille </th>
        <th>Etat de renversement</th>
        <th>Température en °C</th>
        <th>Nombre d'ouverture</th>
      </tr>
    </thead>
    <tbody>

    <?php foreach ($result as $hydrant) { ?>
      <?php $nbOuverture = 0;  // Variable pour compter le nombre d'ouverture sur l'hydrant
      $EtatRenversement = "Non renverser";

      ?>
      <!-- Compter le nombre de typeMess = ouverture pour avoir le nombre d'ouverture -->
      <?php foreach ($result as $data) { ?>
        <?php if ($data['typeMess'] === 'ouverture') {
          $nbOuverture++;
        } ?>

        <!-- Si l'état de l'hydrant est 0 mettre non renverser si non mettre hydrant renverser -->
        <?php if ($data['renversé'] === '0') {
          $EtatRenversement = "Non renversé";
        } else {
          $EtatRenversement = "Hydrant renversé";
        } ?>

      <?php } ?>
      
      
      <tr>
        <td><?= $hydrant['date'] ?></td>
        <td class="typeMess"><?= $hydrant['typeMess'] ?></td>
        <td class="batterie"><?= $hydrant['Batterie'] ?></td>
        <td class="actifVeille"><?= $hydrant['actifVeille'] ?></td>
        <td class="etatRenversement"><?= $EtatRenversement ?></td>
        <td class="temperature"><?= $hydrant['temperature'] ?></td>
        <td><?= $nbOuverture ?></td>
      </tr>
      <?php } ?>
    </tbody>
  </table>
  <form action="http://hydrantconnecter.local/ChydrantsCarte/index"><input type="submit" value="Revenir a la liste des hydrants" id="BtnListeHydrants"></form>
</article>


<script src="https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/leaflet.js"></script>
<script src="<?= base_url("js/detail.js") ?>"></script>