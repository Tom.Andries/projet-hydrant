                <article>
                    <h1>Le projet des hydrants connectée</h1>
                    <p>
                        <img src="<?= base_url("images/PoteauIncendie.jfif") ?>" alt="Hydrant connecté" class="imgfloat" />
                    </p>
                    <p>
                    Le projet consiste à contrôler l’état des poteaux d’incendie (hydrants) en temps réel et à distance pour éviter
                    Le vandalisme de bouche d'incendie, appelé en anglais « street pooling » consiste à ouvrir un 
                    hydrant en dehors de cas d'incendie, généralement en cas de forte chaleur, 
                    afin de répandre l'eau dans les rues. 
                    Ce phénomène tend à s'amplifier ces dernières années.<br><br>

                        Cette pratique est illégale et comporte des risques :<br>

                        -> Risque de blessures pour les enfants (forte pression, de 60m3/h à 120m3/h)<br>
                        -> Risque de désordres électriques<br>
                        -> Inondations, aquaplanage<br>
                        -> Gaspillage d'eau conséquent pour la collectivité<br>
                        -> Baisse de pression sur les autres hydrants, les moyens d’extinction du feu ne sont plus opérationnels<br><br>
                        La lutte contre les incendies s’inscrit dans le cadre des pouvoirs de police administrative 
                        du maire de la commune. Il doit s’assurer de l’existence, du caractère fonctionnel et 
                        de la suffisance des moyens de lutte contre les feux. Cette obligation concerne 
                        en particulier le fait de veiller à la disponibilité constante de points d’eau tels que 
                        des réservoirs et des bornes à incendie auquel cas la responsabilité civile de la commune 
                        sera engagée devant la juridiction administrative (article L. 2216-2 du code général 
                        des collectivités territoriales).
                    </p>
                </article>