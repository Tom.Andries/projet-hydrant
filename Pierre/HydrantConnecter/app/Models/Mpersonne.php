<?php

namespace App\Models;

use CodeIgniter\Model;

class Mpersonne extends Model
{
    protected $table = 'personne';
    protected $primaryKey = 'idContact';
    protected $returnType = 'array';

    public function getAll()
    {
        $requete = $this->select('*');
        return $requete->findAll();                                         // 20 lignes par page
    }

    public function Connexion($data)
    {
        //Récuperation des données
        $mail = $data["mail"];
        $passwordEntre = $data['pass'];

        //Requete SQL "recuperation des informations de l'utilisateur qui à entré son login"
        $requete = $this->select('*')
            ->where(['mail' => $mail]);

        //Reception du résultat de la requete    
        $resultatRequete = $requete->findAll();

        //Compte le nombre de résultats trouvés
        $longeurRequete = count($resultatRequete);

        //Si la requete nous retourne des resultats
        if ($longeurRequete != 0) {

            //Vérification du mot de passe crypter
            $password = $resultatRequete[0]['pass'];
            if (password_verify($passwordEntre, $password)) {

                //Retourne les informations de l'utilisateur
                $retour = $resultatRequete;
            } else {

                //Code d'erreur 
                $retour = -2;
            }
        } else {

            //Code d'erreur 
            $retour = -1;
        }

        return json_encode($retour);
    }
}
