<?php

namespace App\Models;

use CodeIgniter\Model;

class Mhydrants extends Model
{
    protected $table = 'hydrant';
    protected $primaryKey = 'idHydrant';
    protected $returnType = 'array';


    // parti récup données bdd

    public function getAll()
    {
        $requete = $this->select('*');
        return $requete->findAll();
    }

    public function getDetail($prmId)
    {
        $requete = $this->select('*')                       //recup de tout les données de la table hydrants et de la table donnee 
            ->join('donnee', 'hydrant.idHydrant = donnee.idHydrant', 'left')
            ->where(['hydrant.idHydrant' => $prmId])
            ->orderBy('donnee.idData', 'desc');
        return $requete->findAll();
    }

    // Parti pour l'ajax modifier tableau
    
    public function getInitAjax()
    {
            $requete = $this->select('*')                  
            ->join('donnee', 'hydrant.idHydrant = donnee.idHydrant', 'left')
            /* ->where(['hydrant.idHydrant' => 'donnee.idHydrant']) */
            ->orderBy('donnee.idData', 'desc')
            ->groupBy('donnee.idHydrant');
        return $requete->findAll();
    }

    public function getMajAjax($prmDateDernierMaj)
    {
        $requete = $this->select('*')                       
            ->join('donnee', 'hydrant.idHydrant = donnee.idHydrant', 'left')
            ->where('donnee.date >', $prmDateDernierMaj)
            ->orderBy('hydrant.idHydrant', 'desc');
        return $requete->findAll();
    } 

    //Parti calcul volume d'eau

    public function getSommeByIdHydrants($prmId, $prmDate)
    {
        $month = date("m",strtotime($prmDate));
        $requete = $this->select('SUM(`duree`)*hydrant.débitNominal')                       
            ->join('donnee', 'hydrant.idHydrant = donnee.idHydrant', 'left')
            ->where(['hydrant.idHydrant'=> $prmId])
            ->and(['MONTH(date)'=> $month]);
        return $requete->findAll();        
    }


    //Gaulthier commente ta merde 

    public function getAllByidhydrants($prmId)
    {
        $requete = $this->select('*')
            ->like(['idHydrant' => $prmId]);
        return $requete->paginate(10);
    }

    public function getAllByInstallOK()
    {

        $requete = $this->select('*')
            ->like(['installOK' => 0]);
        return $requete->paginate(10);
    }

    public function AddHydrant($prmData)
    {
        $this->allowedFields = ['adresse', 'lat', 'long', 'débitNominal'];
        $this->insert($prmData);
        $this->delete;
        $retour['lastInsertId'] = $this->insertID('idHydrant');
        return $retour;
    }
}
