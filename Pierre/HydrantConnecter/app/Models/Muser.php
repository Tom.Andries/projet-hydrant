<?php

namespace App\Models;

use CodeIgniter\Model;

class Muser extends Model
{
    protected $table = 'personne';
    protected $allowedFields = ['nom', 'mail', 'pass'];
}
