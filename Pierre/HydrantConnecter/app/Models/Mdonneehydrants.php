<?php

namespace App\Models;

use CodeIgniter\Model;

class Mdonneehydrants extends Model
{
    protected $table = 'donnee';
    protected $primaryKey = 'idData';
    protected $returnType = 'array';

    public function getAll()
    {
        $requete = $this->select('*');
        return $requete->findAll();                                         // 20 lignes par page
    }

    public function getDetail($prmId)
    {
        $requete = $this->where(['Id' => $prmId]);
        return $requete->findAll();
    }


    //////////////////////////////////////////////////////////////////////

    public function getAllByidhydrants($prmId)
    {
        $requete = $this->select('*')
            ->like(['idHydrant' => $prmId]);
        return $requete->paginate(10);
    }
}
