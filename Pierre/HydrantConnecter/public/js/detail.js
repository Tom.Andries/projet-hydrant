$(document).ready(function () {
    $('#tableDetail').DataTable({

        language: {

            url: "https://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json"

        },

        "order": [[ 0, "desc" ]] // Ordonner le tableau avec les dates

    });
});

/*CREATION DE LA CARTE */

let Lat = [];
let Long = [];
let marker;
let carte;

var Hydrant = L.icon({
    iconUrl: '/images/PoteauIncendie.png', // Design du marqueur
    iconSize: [20, 40], // Taille du marqueur
    iconAnchor: [6, 31], // Point du marqueur
    popupAnchor: [-3, -76] // Distance entre le marqueur et le popup
});

/* Variable pour la position d'un hydrants (center sur la map) */
let url = document.location.href; // récuper l'url 
let pos = url.lastIndexOf("/");   // savoir ou est le dernier slash de l'url
let idHydrants = url.substr(pos + 1); // enlever tout avant le dernier slash +1 pour enlever le slash


initmap();
marqueur();
alerte();
positionHydrants();



/*REQUETE AJAX POUR RECUPERER LA LISTE DES HYDRANT */

function marqueur() {
    $.ajax({

        type: "GET",
        url: 'http://hydrantconnecter.local/Api/hydrants',
        dataType: "json",
        json: "json",
        success: OnGetMarqueur,
    });
    /* Ajout des marqueur sur la carte (position des hydrants) */
    function OnGetMarqueur(reponse) {
        for (let i = 0; i < reponse.length; i++) {
            Lat[i] = reponse[i]['lat'];                               // récup la lat d'un hydrant
            Long[i] = reponse[i]['long'];                             // récup la long d'un hydrant
            marker = L.marker([Lat[i], Long[i]], { icon: Hydrant }).addTo(carte);         //Ajouter un marqueur à la lat et long d'un hydrant
            marker.bindPopup(`Hydrant Connectés numéro sigfox : ${reponse[i]['codeSigfox']} <br> <a href="http://hydrantconnecter.local/ChydrantsCarte/detail/${reponse[i]['idHydrant']}">Plus d'information</a> `);  // Popup avec le numéro Sigfox de l'hydrant et le plus de détail
           }
    }
}

function initmap() {
    carte = L.map('macarte', {
        minZoom: 8, // Le zoom min (dézoom)
    }).setView([50.689500, 2.8900000], 14);
    L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
        attribution: 'HydrantMap'
    }).addTo(carte);
}

function alerte(){
// Mettre la case en rouge si le typeMess est ouverture ou renverser
var typeMess = document.getElementsByClassName("typeMess");
for (let i = 0; i < typeMess.length; i++) {
    if ((typeMess[i].innerHTML == "ouverture") || (typeMess[i].innerHTML == "renversement")) {
        typeMess[i].classList.add("alerte");
    }
}

// Mettre la case en rouge si la batterie est inférieure à 20 %
var batterie = document.getElementsByClassName("batterie");
for (let i = 0; i < batterie.length; i++) {
    if (batterie[i].innerHTML < 20 && batterie[i].innerHTML > 0 ) {
        batterie[i].classList.add("alerte");
    }
}

// Mettre la case en rouge si l'hydrant est renversé
var etatRenversement = document.getElementsByClassName("etatRenversement");
for (let i = 0; i < etatRenversement.length; i++) {
    if (etatRenversement[i].innerHTML == "Hydrant renversé" ) {
        etatRenversement[i].classList.add("alerte");
    }
}

// Mettre la case en rouge si la température est trop basse
var temperature = document.getElementsByClassName("temperature");
for (let i = 0; i < temperature.length; i++) {
    if (temperature[i].innerHTML < 0 ) {
        temperature[i].classList.add("alerte");
    }
}

}

/*REQUETE AJAX POUR RECUPERER la position d'un hydrants */

function positionHydrants() {
    $.ajax({

        type: "GET",
        url: 'http://hydrantconnecter.local/Api/hydrants/'+idHydrants,
        dataType: "json",
        json: "json",
        success: OnGetSuccessPosition,
    });
    /* Ajout des marqueur sur la carte (position des hydrants) */
    function OnGetSuccessPosition(reponse) {
        console.log(reponse);
            reponse[0]['lat'];                               // récup la lat de l'hydrant
            reponse[0]['long'];                             // récup la long de l'hydrant
        carte.setView([reponse[0]['lat'],reponse[0]['long']],18);
        
    }
}