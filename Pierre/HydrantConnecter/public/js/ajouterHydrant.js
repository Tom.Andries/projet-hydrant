$("#btnAjouter").on("click", function () {
    console.log("test")
        let adresseVal = $("#adresse").val(), // récupère l'adresse
            latVal = parseFloat($("#lat").val()), // récupère la latitude
            longVal = parseFloat($("#long").val()), // récupère la longitude
            debitNominalVal = parseInt($("#debitNominal").val()), // récupère le débit nominal
            jsonPayload = { // On met les valeurs dans un objet
                "adresse": adresseVal,
                "lat": latVal,
                "long": longVal,
                "débitNominal": debitNominalVal
            };
    
        jsonPayload = JSON.stringify(jsonPayload); // Passage au format json
    
        // Requete ajax pour ajouter un hydrant
        $.ajax({
            method: "POST",
            url: "http://hydrantconnecter.local/Api/Hydrants",
            data: { dto: jsonPayload },
            dataType: "json",
            sucess: onPostHydrantSucess,
            error: onPostHydrantError
        });
    
        function onPostHydrantSucess(response) {
            console.log(response);
        }
    
        function onPostHydrantError(error) {
            console.log(error);
        }
    });