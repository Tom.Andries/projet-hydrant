

let dateDerniereMaj = getDate();

setInterval(majTableau, 20000);

function getDate() {

    let dateDuJour

    var date = new Date;

    var seconds = date.getSeconds();
    var minutes = date.getMinutes();
    var hour = date.getHours();

    var year = date.getFullYear();
    var month = date.getMonth() + 1; // beware: January = 0; February = 1, etc.
    var day = date.getDate();

    if (month < 10) month = '0' + month;
    if (day < 10) day = '0' + day;
    if (hour < 10) hour = '0' + hour;
    if (minutes < 10) minutes = '0' + minutes;
    if (seconds < 10) seconds = '0' + seconds;

    dateDuJour = year + '-' + month + '-' + day + ' ' + hour + ':' + minutes + ':' + seconds;

    return dateDuJour;
}


function majTableau() {

    $.ajax({
        method: "GET",
        url: "http://hydrantconnecter.local/Api/Hydrants/majTableau/" + dateDerniereMaj,
        dataType: "json",
        success: onGetMajTableauSuccess,
        error: onGetMajTableauError
    });

    function onGetMajTableauSuccess(reponse) {

        let trEl = '';
        let etatRenversement = '';
        console.log(dateDerniereMaj);
        dateDerniereMaj = getDate();


        for (let i = 0; i < reponse.length; i++) {
            trEl = $(`tr[data-id='${reponse[i].idHydrant}']`);
            console.log($(trEl).find(".typeMess").val());


            if (reponse[i].renversé === '0') {
                etatRenversement = "Non renversé";
            } else {
                etatRenversement = "Hydrant renversé";
            }
            $(trEl).find(".typeMess").text(reponse[i].typeMess)
            $(trEl).find(".batterie").text(reponse[i].Batterie)
            $(trEl).find(".actifVeille").text(reponse[i].actifVeille)
            $(trEl).find(".etatRenversement").text(etatRenversement)
            $(trEl).find(".temperature").text(reponse[i].temperature)
        }
        alerte();
    }

    function onGetMajTableauError(error) {
        console.log(error);
    }
}

function alerte() {
    // Mettre la case en rouge si le typeMess est ouverture ou renverser
    var typeMess = document.getElementsByClassName("typeMess");
    for (let i = 0; i < typeMess.length; i++) {
        if ((typeMess[i].innerHTML == "ouverture") || (typeMess[i].innerHTML == "renversement")) {
            typeMess[i].classList.add("alerte");
        }else{
            typeMess[i].classList.remove("alerte")
        }
    }

    // Mettre la case en rouge si la batterie est inférieure à 20 %
    var batterie = document.getElementsByClassName("batterie");
    for (let i = 0; i < batterie.length; i++) {
        if (batterie[i].innerHTML < 20 && batterie[i].innerHTML > 0) {
            batterie[i].classList.add("alerte");
        }else{
            batterie[i].classList.remove("alerte")
        }
    }

    // Mettre la case en rouge si l'hydrant est renversé
    var etatRenversement = document.getElementsByClassName("etatRenversement");
    for (let i = 0; i < etatRenversement.length; i++) {
        if (etatRenversement[i].innerHTML == "Hydrant renversé") {
            etatRenversement[i].classList.add("alerte");
        }else{
            etatRenversement[i].classList.remove("alerte")
        }
    }

    // Mettre la case en rouge si la température est trop basse
    var temperature = document.getElementsByClassName("temperature");
    for (let i = 0; i < temperature.length; i++) {
        if (temperature[i].innerHTML < 0) {
            temperature[i].classList.add("alerte");
        }else{
            temperature[i].classList.remove("alerte")
        }
    }

}