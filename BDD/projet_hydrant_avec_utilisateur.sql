-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  jeu. 18 fév. 2021 à 13:03
-- Version du serveur :  5.7.26
-- Version de PHP :  7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `projet_hydrant`
--

-- --------------------------------------------------------

--
-- Structure de la table `commune`
--

DROP TABLE IF EXISTS `commune`;
CREATE TABLE IF NOT EXISTS `commune` (
  `idCommune` int(8) NOT NULL AUTO_INCREMENT,
  `nomCommune` varchar(25) NOT NULL,
  `lat` float NOT NULL,
  `long` float NOT NULL,
  PRIMARY KEY (`idCommune`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `donnee`
--

DROP TABLE IF EXISTS `donnee`;
CREATE TABLE IF NOT EXISTS `donnee` (
  `idData` int(8) UNSIGNED NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `typeMess` enum('ouverture','fermeture','renversement','activation','veille') NOT NULL,
  `angle` float NOT NULL,
  `temperature` float NOT NULL,
  `volume` float NOT NULL COMMENT 'calculé à la fermeture en fonction de la durée d''ouverture et du débit nominal',
  `duree` int(11) NOT NULL COMMENT 'calculé à la fermeture depuis la dernière ouverture',
  `idHydrant` int(8) NOT NULL,
  PRIMARY KEY (`idData`),
  KEY `fk_donne` (`idHydrant`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `hydrant`
--

DROP TABLE IF EXISTS `hydrant`;
CREATE TABLE IF NOT EXISTS `hydrant` (
  `idHydrant` int(8) NOT NULL AUTO_INCREMENT,
  `codeSigfox` int(11) NOT NULL,
  `adresse` varchar(50) NOT NULL,
  `lat` float NOT NULL,
  `long` float NOT NULL,
  `débitNominal` float NOT NULL,
  `actifVeille` enum('actif','veille') NOT NULL COMMENT 'recopie de la donnée quand on reçoit une trame actif/veille',
  `renversé` tinyint(1) NOT NULL COMMENT 'enregistrement de l''état renversement avec angle > 8°',
  `installOK` tinyint(1) NOT NULL,
  `dateFirstActivation` date NOT NULL,
  `idCommune` int(8) NOT NULL,
  PRIMARY KEY (`idHydrant`),
  KEY `fk_hydrant` (`idCommune`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `personne`
--

DROP TABLE IF EXISTS `personne`;
CREATE TABLE IF NOT EXISTS `personne` (
  `idContact` int(8) NOT NULL AUTO_INCREMENT,
  `nom` varchar(25) NOT NULL,
  `fonction` enum('maire','employé','technicien') NOT NULL,
  `telAlerte` varchar(10) NOT NULL,
  `mail` varchar(30) NOT NULL,
  `login` varchar(30) NOT NULL,
  `pass` varchar(20) NOT NULL,
  `typeAlerte` enum('aucune','sms','mail','sms/mail') NOT NULL,
  `idCommune` int(8) NOT NULL,
  PRIMARY KEY (`idContact`),
  KEY `fk_personne` (`idCommune`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
