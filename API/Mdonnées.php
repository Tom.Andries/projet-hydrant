<?php
public function createConteneur($prmData)
{
    //nom des colonnes qui peuvent être modifiées par cette requête
    //(obligatoire pour les requêtes INSERT et UPDATE)
    $this->allowedFields = ['AddrEmplacement', 'LatLng', 'VolumeMax'];
    $this->insert($prmData);

    //ID du nouvel enregistrement retourné dans un tableau associatif
    $retour['lastInsertId'] = $this->insertID('Id');
    return $retour;
}
