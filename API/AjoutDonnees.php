<?php

namespace App\Controllers\Api;

use CodeIgniter\RESTful\ResourceController;

class Donnees extends ResourceController
{
    //protected $modelName = 'App\Models\Mconteneur';
    protected $model;
    protected $format = 'json';

    public function index()
    {
        $result = $this->model->getAll();
        return $this->respond($result);
    }

    public function show($id = null)
    {
        $result = $this->model->getDetail($id);
        if (count($result) == 0) {
            return $this->respond($result, 400);
        } else {
            return $this->respond($result);
        }
    }

    public function create()
    {
        $data = $this->request->getPost('dto'); 
        $data = json_decode($data, true);

        if ($data == "") {
            $valRetour = $this->respond("", 400);
        } else {
            // ajout des données $result = $this->model->createConteneur($data);
            $valRetour = $this->respond($result, 201);
        }
        return $valRetour;
    }
}
